package sidm.com.gcollector.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import sidm.com.gcollector.Player.SaveData;
import sidm.com.gcollector.R;

// Author: Hui Sheng
// Splash Activity, this is the default activity that the application starts
// Alpha fades in over time
public class SplashPage extends Activity {

    private final long m_SplashDelay = 5000; // Delay in milliseconds before going MainPage
    private final Handler m_Handler = new Handler(); // Handler for calling functions after X seconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashpage);

        // Load Save Data
        SaveData.getInstance();

        // Run functions after X seconds
        m_Handler.postDelayed(goMainMenuRunnable, m_SplashDelay);
        m_Handler.postDelayed(alphaChangeRunnable, 0);

        // Change to MainPage on click to skip Splash Screen
        RelativeLayout layout = findViewById(R.id.splashLayout);
        layout.setAlpha(0); // Hide the background first

        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                goMainMenuRunnable.run();

                // Stop the threads
                m_Handler.removeCallbacks(goMainMenuRunnable);
                m_Handler.removeCallbacks(alphaChangeRunnable);
            }
        });

    }

    // Goes to MainPage when the player taps on the screen
    private Runnable goMainMenuRunnable = new Runnable() {
        @Override
        public void run() {
            startActivity(new Intent(getBaseContext(), MainPage.class));
            finish();
        }
    };

    // Increment the splash screen alpha every X seconds for "Fade-In" effect
    private Runnable alphaChangeRunnable = new Runnable() {
        @Override
        public void run() {
            RelativeLayout layout = findViewById(R.id.splashLayout);
            layout.setAlpha(layout.getAlpha() + 0.01f);

            // Keep running until we alpha is fully showed
            if (layout.getAlpha() < 1.f)
                m_Handler.postDelayed(alphaChangeRunnable, 10);
        }
    };

}
