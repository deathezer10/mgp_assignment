package sidm.com.gcollector.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.facebook.share.Share;

import sidm.com.gcollector.IO.TouchManager;
import sidm.com.gcollector.Dialogs.QuitConfirmDialogFragment;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;

// Author: Hui Sheng
// Re-director activity, responsible for showing the GameView
public class GamePage extends Activity {

    Animation btnAnim;
    private long mLastClickTime = 0; // Last time that the button was clicked to prevent double clicking
    int dialogCount = 0;
    @Override
    public void onBackPressed() {
        // Do nothing on back, prevents player from accidentally closing the game
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.gameplayhud);
        SceneGamePlay.Instance.m_Context = this;

        // Exit button listener
        btnAnim = AnimationUtils.loadAnimation(this, R.anim.btn_bounce);

        findViewById(R.id.btnPause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SceneGamePlay.Instance.m_IsGamePaused || SceneGamePlay.Instance.IsGameOver())
                    return;

                // Button Animation
                view.startAnimation(btnAnim);
                SceneGamePlay.Instance.m_IsGamePaused = true;
            }
        });

        // Lose Panel Retry button listener
        findViewById(R.id.btnRetry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Button Animation
                view.startAnimation(btnAnim);
                SceneGamePlay.Instance.Terminate();
                recreate(); // restart the activity
            }
        });

        // Lose Panel Exit button listener
        findViewById(R.id.btnQuit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Button Animation
                view.startAnimation(btnAnim);
                finish();
            }
        });


            //Dialog btn
            findViewById(R.id.btnDialog).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Button Animation
                    ImageView img = findViewById(R.id.textDialog);
                    img.setImageResource(R.drawable.text_2);
                    view.startAnimation(btnAnim);
                    dialogCount += 1;
                    if (dialogCount == 3) {
                        SceneGamePlay.Instance.dialogState = false;
                        RelativeLayout dialog = findViewById(R.id.DialogPanel);
                        dialog.setVisibility(View.GONE);
                    }

                }
            });



        // Win Panel Go Next button listener
        findViewById(R.id.btnGoNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Button Animation
                view.startAnimation(btnAnim);
                SceneGamePlay.Instance.Terminate();

                // Next level
                if (SceneGamePlay.Instance.m_CurrentLevel < 2)
                    SceneGamePlay.Instance.m_CurrentLevel++;

                recreate(); // restart the activity
            }
        });

        // Win Panel Exit button listener
        findViewById(R.id.btnQQuit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Button Animation
                view.startAnimation(btnAnim);
                finish();
            }
        });

        // Pause Panel Resume button listener
        findViewById(R.id.btnResume).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Button Animation
                SceneGamePlay.Instance.m_IsGamePaused = false;
            }
        });

        // Pause Panel Exit button listener
        findViewById(R.id.btnPauseQuit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Button Animation
                view.startAnimation(btnAnim);

                // Intent myIntent = new Intent(this, ShareScorePage.class);
                // startActivity(myIntent);

                if (!QuitConfirmDialogFragment.IsShown) {
                    QuitConfirmDialogFragment quitConfirm = new QuitConfirmDialogFragment();
                    quitConfirm.show(getFragmentManager(), "Quit Confirm");
                }
            }
        });

        // Parry button listener
        findViewById(R.id.btnParry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                SceneGamePlay.Instance.m_PlayerInfo.SpawnShield();
                findViewById(R.id.btnParry).setVisibility(View.INVISIBLE);

                // Button Animation
                view.startAnimation(btnAnim);
            }
        });

        // Multi-shot button listener
        findViewById(R.id.btnMultishot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                SceneGamePlay.Instance.m_PlayerInfo.SpawnMultiShot();

                // Button Animation
                view.startAnimation(btnAnim);
            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int x = (int) event.getX();
        int y = (int) event.getY();

        // Post touch updates to TouchManager
        TouchManager.Instance.Update(x, y, event.getAction());

        return true;
    }
}
