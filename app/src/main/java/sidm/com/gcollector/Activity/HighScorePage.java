package sidm.com.gcollector.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import sidm.com.gcollector.Player.SaveData;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;

// Author: Hui Sheng
// Highscore Activity, HighScore values are pulled once during OnCreate()
public class HighScorePage extends Activity implements View.OnClickListener {

    // Button IDs
    private Button btnBack;
    private Button btnShareScore;

    private final Handler m_Handler = new Handler(); // Handler for calling functions after X seconds
    private boolean b_ActionTriggered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.highscoremenu);

        // Set click listener to all buttons
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        btnShareScore = findViewById(R.id.btnShareScore);
        btnShareScore.setOnClickListener(this);

        // Populate the ScrollView with the player's upgrades
        PopulateScrollView();

    }

    private void PopulateScrollView() {

        LinearLayout linearLayout = findViewById(R.id.scrollUpgrade);

        // 1st
        TextView lblFirst = CreateScrollviewLabel(Color.WHITE);
        lblFirst.setText(R.string.highscoremenu_first);
        linearLayout.addView(lblFirst);

        TextView lblFirst_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Health) ? Color.GREEN : Color.RED);
        lblFirst_Query.setText(String.valueOf(SaveData.getInstance().m_Highscore_1));
        linearLayout.addView(lblFirst_Query);

        // 2nd
        TextView lblSecond = CreateScrollviewLabel(Color.WHITE);
        lblSecond.setText(R.string.highscoremenu_second);
        linearLayout.addView(lblSecond);

        TextView lblSecond_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Power) ? Color.GREEN : Color.RED);
        lblSecond_Query.setText(String.valueOf(SaveData.getInstance().m_Highscore_2));
        linearLayout.addView(lblSecond_Query);

        // 3rd
        TextView lblThird = CreateScrollviewLabel(Color.WHITE);
        lblThird.setText(R.string.highscoremenu_third);
        linearLayout.addView(lblThird);

        TextView lblThird_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Parry) ? Color.GREEN : Color.RED);
        lblThird_Query.setText(String.valueOf(SaveData.getInstance().m_Highscore_3));
        linearLayout.addView(lblThird_Query);

        // 4th
        TextView lblFourth = CreateScrollviewLabel(Color.WHITE);
        lblFourth.setText(R.string.highscoremenu_fourth);
        linearLayout.addView(lblFourth);

        TextView lblFourth_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Bottle) ? Color.GREEN : Color.RED);
        lblFourth_Query.setText(String.valueOf(SaveData.getInstance().m_Highscore_4));
        linearLayout.addView(lblFourth_Query);

        // 5th
        TextView lblFifth = CreateScrollviewLabel(Color.WHITE);
        lblFifth.setText(R.string.highscoremenu_fifth);
        linearLayout.addView(lblFifth);

        TextView lblFifth_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Regen) ? Color.GREEN : Color.RED);
        lblFifth_Query.setText(String.valueOf(SaveData.getInstance().m_Highscore_5));
        linearLayout.addView(lblFifth_Query);
    }

    // Returns a TextView to be used for the ScrollView elements
    private TextView CreateScrollviewLabel(int color) {

        // Default label setup
        TextView label = new TextView(this);
        label.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        label.setTextColor(color);
        label.setGravity(View.TEXT_ALIGNMENT_CENTER);
        label.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        label.setGravity(TextView.TEXT_ALIGNMENT_CENTER);

        // Convert px to dp
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int dp = (int) ((50 / displayMetrics.density) + 0.5);

        label.setPadding(dp, dp, dp, dp);
        return label;
    }

    @Override
    public void onClick(View v) {

        // Prevent player from spamming the same button
        if (b_ActionTriggered)
            return;
        else
            b_ActionTriggered = true;

        // Button Animation
        final Animation btnAnim = AnimationUtils.loadAnimation(this, R.anim.btn_bounce);

        if (btnBack == v) {
            btnBack.startAnimation(btnAnim);
            m_Handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish(); // Go back main menu
                    b_ActionTriggered = false;
                }
            }, btnAnim.getDuration());

        } else if (btnShareScore == v) { // Facebook share
            btnShareScore.startAnimation(btnAnim);
            m_Handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), ShareScorePage.class);
                    startActivity(intent);

                    b_ActionTriggered = false;
                }
            }, btnAnim.getDuration());

        }

    }

}


