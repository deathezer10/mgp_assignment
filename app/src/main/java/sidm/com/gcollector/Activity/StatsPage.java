package sidm.com.gcollector.Activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import sidm.com.gcollector.Player.SaveData;
import sidm.com.gcollector.R;

// Author: Hui Sheng
// Player's Upgrade Activity, only display which Upgrades that the player has obtained
public class StatsPage extends Activity implements View.OnClickListener {

    // Button IDs
    private Button btnBack;

    private final Handler m_Handler = new Handler(); // Handler for calling functions after X seconds
    private boolean b_ActionTriggered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.statsmenu);

        // Set click listener to all buttons
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        // Populate the ScrollView with the player's upgrades
        PopulateScrollView();

    }

    private void PopulateScrollView() {

        LinearLayout linearLayout = findViewById(R.id.scrollUpgrade);

        // Health
        TextView lblHealth = CreateScrollviewLabel(Color.WHITE);
        lblHealth.setText(R.string.statsmenu_upgrade_health);
        linearLayout.addView(lblHealth);

        TextView lblHealth_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Health) ? Color.GREEN : Color.RED);
        lblHealth_Query.setText((SaveData.getInstance().b_Upgrade_Health) ? R.string.statsmenu_upgrade_valid : R.string.statsmenu_upgrade_invalid);
        linearLayout.addView(lblHealth_Query);

        // Power
        TextView lblPower = CreateScrollviewLabel(Color.WHITE);
        lblPower.setText(R.string.statsmenu_upgrade_power);
        linearLayout.addView(lblPower);

        TextView lblPower_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Power) ? Color.GREEN : Color.RED);
        lblPower_Query.setText((SaveData.getInstance().b_Upgrade_Power) ? R.string.statsmenu_upgrade_valid : R.string.statsmenu_upgrade_invalid);
        linearLayout.addView(lblPower_Query);

        // Parry
        TextView lblParry = CreateScrollviewLabel(Color.WHITE);
        lblParry.setText(R.string.statsmenu_upgrade_parry);
        linearLayout.addView(lblParry);

        TextView lblParry_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Parry) ? Color.GREEN : Color.RED);
        lblParry_Query.setText((SaveData.getInstance().b_Upgrade_Parry) ? R.string.statsmenu_upgrade_valid : R.string.statsmenu_upgrade_invalid);
        linearLayout.addView(lblParry_Query);

        // Bottle
        TextView lblBottle = CreateScrollviewLabel(Color.WHITE);
        lblBottle.setText(R.string.statsmenu_upgrade_bottle);
        linearLayout.addView(lblBottle);

        TextView lblBottle_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Bottle) ? Color.GREEN : Color.RED);
        lblBottle_Query.setText((SaveData.getInstance().b_Upgrade_Bottle) ? R.string.statsmenu_upgrade_valid : R.string.statsmenu_upgrade_invalid);
        linearLayout.addView(lblBottle_Query);

        // Regen
        TextView lblRegen = CreateScrollviewLabel(Color.WHITE);
        lblRegen.setText(R.string.statsmenu_upgrade_regen);
        linearLayout.addView(lblRegen);

        TextView lblRegen_Query = CreateScrollviewLabel((SaveData.getInstance().b_Upgrade_Regen) ? Color.GREEN : Color.RED);
        lblRegen_Query.setText((SaveData.getInstance().b_Upgrade_Regen) ? R.string.statsmenu_upgrade_valid : R.string.statsmenu_upgrade_invalid);
        linearLayout.addView(lblRegen_Query);
    }

    // Returns a TextView to be used for the ScrollView elements
    private TextView CreateScrollviewLabel(int color) {
        TextView label = new TextView(this);
        label.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        label.setTextColor(color);
        label.setGravity(View.TEXT_ALIGNMENT_CENTER);
        label.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        label.setGravity(TextView.TEXT_ALIGNMENT_CENTER);

        // Convert px to dp
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int dp = (int) ((50 / displayMetrics.density) + 0.5);

        label.setPadding(dp, dp, dp, dp);
        return label;
    }

    @Override
    public void onClick(View v) {

        // Prevent player from spamming the same button
        if (b_ActionTriggered)
            return;
        else
            b_ActionTriggered = true;

        // Button Animation
        final Animation btnAnim = AnimationUtils.loadAnimation(this, R.anim.btn_bounce);

        if (btnBack == v) {
            btnBack.startAnimation(btnAnim);
            m_Handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    b_ActionTriggered = false;
                }
            }, btnAnim.getDuration());

        }

    }

}


