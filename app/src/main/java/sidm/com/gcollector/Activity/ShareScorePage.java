package sidm.com.gcollector.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileManager;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import java.util.Arrays;
import java.util.List;

import sidm.com.gcollector.Player.SaveData;
import sidm.com.gcollector.R;

// Author: Hui Sheng
// Floating dialog for sharing HighScore onto Facebook
public class ShareScorePage extends Activity implements View.OnClickListener {

    LoginButton btn_FbLogin;
    ShareButton btn_FbShare;
    ProfilePictureView img_ProfilePicture;
    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private SharePhotoContent shareContent;

    List<String> PERMISSIONS = Arrays.asList("publish_actions");
    Activity m_CurrentActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        FacebookSdk.setApplicationId(getString(R.string.facebook_app_id));
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sharescorepage);

        m_CurrentActivity = this;

        ((TextView) findViewById(R.id.lblCurrentScore)).setText(Long.toString(SaveData.getInstance().m_Highscore_1));

        btn_FbLogin = findViewById(R.id.fb_login_button);
        btn_FbLogin.setOnClickListener(this);

        btn_FbShare = findViewById(R.id.fb_share_highscore);
        btn_FbShare.setOnClickListener(this);

        img_ProfilePicture = findViewById(R.id.fb_profile_picture);
        callbackManager = CallbackManager.Factory.create();

        loginManager = LoginManager.getInstance();
        loginManager.logInWithPublishPermissions(this, PERMISSIONS);

        AccessTokenTracker accessToken = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null) {
                    // Disable profile picture & share content once logged out
                    img_ProfilePicture.setProfileId("");
                    btn_FbShare.setShareContent(null);
                } else {
                    // User might already be logged in, set the content again if so
                    SetUpShareDialog(Profile.getCurrentProfile());
                }
            }
        };
        accessToken.startTracking();

        // Set profile picture on login
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                // User logged in
                // Wait for profile to finish fetching, then set the profile picture and enable share button
                ProfileTracker profileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                        this.stopTracking();
                        Profile.setCurrentProfile(currentProfile);
                        SetUpShareDialog(currentProfile);
                    }
                };
                profileTracker.startTracking();
            }

            @Override
            public void onCancel() {
                System.out.println("Login attempt canceled.");
                loginManager.logOut();
            }

            @Override
            public void onError(FacebookException error) {
                System.out.println("Login attempt failed!");
                loginManager.logOut();
            }

        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // Sets the profile picture and ShareContent to the ShareButton
    private void SetUpShareDialog(Profile currentProfile) {

        if (currentProfile != null) {
            // Set profile picture
            img_ProfilePicture.setProfileId(currentProfile.getId());

            // Create an SharePhoto content
            Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(image)
                    .setCaption("Nice! I just got a HighScore of " + SaveData.getInstance().m_Highscore_1 + " in GarbageCollector!")
                    .build();
            shareContent = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();

            // Set the ShareButton content once logged in
            btn_FbShare.setShareContent(shareContent);
        }

    }

    @Override
    public void onClick(View v) {
        if (v == btn_FbShare) {

            // Show confirmation dialog before sharing the high score
            AlertDialog.Builder alert_builder = new AlertDialog.Builder(ShareScorePage.this);
            alert_builder.setTitle("Facebook Share");
            alert_builder.setMessage("Do you want to share your score of " + SaveData.getInstance().m_Highscore_1 + "?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // Share the content and display a toast
                            ShareApi.share(shareContent, null);
                            Toast.makeText(m_CurrentActivity, "High Score shared successfully!", Toast.LENGTH_SHORT).show();

                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });


            alert_builder.show();

        }
    }


}
