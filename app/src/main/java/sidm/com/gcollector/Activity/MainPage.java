package sidm.com.gcollector.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import sidm.com.gcollector.Player.SaveData;
import sidm.com.gcollector.R;

// Author: Hui Sheng
// Main Menu Activity
public class MainPage extends Activity implements View.OnClickListener {

    // Button IDs
    private Button btnPlay;
    private Button btnStats;
    private Button btnScore;
    private Button btnResetSaveData;
    private Button btnMaxSaveData;

    private final Handler m_Handler = new Handler(); // Handler for calling functions after X seconds
    private boolean b_ActionTriggered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mainmenu);

        // Set click listener to all buttons
        btnPlay = findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(this);

        btnStats = findViewById(R.id.btnStats);
        btnStats.setOnClickListener(this);

        btnScore = findViewById(R.id.btnScore);
        btnScore.setOnClickListener(this);

        btnResetSaveData = findViewById(R.id.btnResetSaveData);
        btnResetSaveData.setOnClickListener(this);

        btnMaxSaveData = findViewById(R.id.btnMaxSaveData);
        btnMaxSaveData.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        // Prevent player from spamming the same button
        if (b_ActionTriggered)
            return;
        else
            b_ActionTriggered = true;

        // Button Animation
        final Animation btnAnim = AnimationUtils.loadAnimation(this, R.anim.btn_bounce);

        if (btnPlay == v) {
            btnPlay.startAnimation(btnAnim);

            // Go GameView
            m_Handler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            intent.setClass(getBaseContext(), WorldPage.class);
                            startActivity(intent);
                            b_ActionTriggered = false;
                        }
                    }, btnAnim.getDuration());

        } else if (btnStats == v) {
            btnStats.startAnimation(btnAnim);

            // Go Stats Menu
            m_Handler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            intent.setClass(getBaseContext(), StatsPage.class);
                            startActivity(intent);
                            b_ActionTriggered = false;
                        }
                    }, btnAnim.getDuration());

        } else if (btnScore == v) {
            btnScore.startAnimation(btnAnim);

            // Reset Save Data button
            m_Handler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            intent.setClass(getBaseContext(), HighScorePage.class);
                            startActivity(intent);
                            b_ActionTriggered = false;
                        }
                    }, btnAnim.getDuration());
        } else if (btnResetSaveData == v) {
            btnResetSaveData.startAnimation(btnAnim);

            // Reset Save Data button
            m_Handler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            SaveData.getInstance().ResetEverything();
                            Toast.makeText(getApplicationContext(), "Save data resetted!", Toast.LENGTH_SHORT).show();
                            b_ActionTriggered = false;
                        }
                    }, btnAnim.getDuration());
        } else if (btnMaxSaveData == v) {
            btnMaxSaveData.startAnimation(btnAnim);

            // Max Save Data button
            m_Handler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            SaveData.getInstance().MaxEverything();
                            Toast.makeText(getApplicationContext(), "Save data maxed!", Toast.LENGTH_SHORT).show();
                            b_ActionTriggered = false;
                        }
                    }, btnAnim.getDuration());
        }

    }

}


