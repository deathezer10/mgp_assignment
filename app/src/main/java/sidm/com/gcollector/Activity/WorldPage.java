package sidm.com.gcollector.Activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import sidm.com.gcollector.Player.SaveData;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;

// Author: Haziq
// Level Selector Activity, levels are unlocked only if the previous level is cleared
public class WorldPage extends Activity implements View.OnClickListener {

    // Button IDs
    private Button btnLvl1;
    private Button btnLvl2;
    private Button btnLvl3;
    private Button btnExit;

    private final Handler m_Handler = new Handler(); // Handler for calling functions after X seconds
    private boolean b_ActionTriggered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.worldpage);

        // Set click listener to all buttons
        btnLvl1 = findViewById(R.id.btnLvl1);
        btnLvl1.setOnClickListener(this);

        btnLvl2 = findViewById(R.id.btnLvl2);
        btnLvl2.setOnClickListener(this);

        btnLvl3 = findViewById(R.id.btnLvl3);
        btnLvl3.setOnClickListener(this);

        btnExit = findViewById(R.id.btnQuitToMain);
        btnExit.setOnClickListener(this);

        // Only show levels that are unlocked
        if (!SaveData.getInstance().b_Level_2)
            btnLvl2.setVisibility(View.INVISIBLE);
        if (!SaveData.getInstance().b_Level_3)
            btnLvl3.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {

        // Prevent player from spamming the same button
        if (b_ActionTriggered)
            return;
        else
            b_ActionTriggered = true;

        // Button Animation
        final Animation btnAnim = AnimationUtils.loadAnimation(this, R.anim.btn_bounce);

        if (btnLvl1 == v) {
            btnLvl1.startAnimation(btnAnim);

            m_Handler.postDelayed( // Level 1
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            SceneGamePlay.Instance.m_CurrentLevel = 0;
                            intent.setClass(getBaseContext(), GamePage.class);
                            startActivity(intent);
                            b_ActionTriggered = false;
                            finish(); // Prevent going back
                        }
                    }, btnAnim.getDuration());

        } else if (btnLvl2 == v) {
            btnLvl2.startAnimation(btnAnim);

            m_Handler.postDelayed( // Level 2
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            SceneGamePlay.Instance.m_CurrentLevel = 1;
                            intent.setClass(getBaseContext(), GamePage.class);
                            startActivity(intent);
                            b_ActionTriggered = false;
                            finish(); // Prevent going back
                        }
                    }, btnAnim.getDuration());

        } else if (btnLvl3 == v) { // Level 3 - Boss
            btnLvl3.startAnimation(btnAnim);

            m_Handler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            SceneGamePlay.Instance.m_CurrentLevel = 2;
                            intent.setClass(getBaseContext(), GamePage.class);
                            startActivity(intent);
                            b_ActionTriggered = false;
                            finish(); // Prevent going back
                        }
                    }, btnAnim.getDuration());

        } else if (btnExit == v) { // Exit to main menu
            btnExit.startAnimation(btnAnim);

            // Go back to main menu
            m_Handler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            intent.setClass(getBaseContext(), MainPage.class);
                            startActivity(intent);
                            b_ActionTriggered = false;
                            finish();
                        }
                    }, btnAnim.getDuration());
        }

    }

}
