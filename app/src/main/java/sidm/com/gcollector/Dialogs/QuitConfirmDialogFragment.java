package sidm.com.gcollector.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;


// Author: Haziq
// Quit confirmation dialog for the in-game quit button
public class QuitConfirmDialogFragment extends DialogFragment {

    public static boolean IsShown = false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        IsShown = true;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Confirm quit game?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                IsShown = false;
                getActivity().finish(); // Quit game
            }
        }).setNegativeButton("No, I love this game", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                IsShown = false;
            }
        });


        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog){
        IsShown = false;
    }

    @Override
    public void onDismiss(DialogInterface dialog){
        IsShown = false;
    }

}
