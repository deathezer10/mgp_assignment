package sidm.com.gcollector.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.UpdateThread;


// Author: Hui Sheng
// Handles the spawning of the game's worker thread and rendering of surface
public class GameView extends SurfaceView {

    private SurfaceHolder holder = null;

    private UpdateThread updateThread;

    // For Custom View in the FrameLayout
    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitView();
    }

    public GameView(Context _context) {
        super(_context);
        InitView();
    }

    void InitView() {
        holder = getHolder();
        updateThread = new UpdateThread(this);

        if (holder != null) {

            holder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    // Set up
                    if (!updateThread.IsRunning())
                        updateThread.Initialize();

                    if (!updateThread.isAlive())
                        updateThread.start();

                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                    // Update thread will handle this
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    AudioManager.Instance.Exit();
                    updateThread.Terminate();
                }
            });
        }

    }

}
