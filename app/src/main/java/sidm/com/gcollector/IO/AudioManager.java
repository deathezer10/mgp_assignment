package sidm.com.gcollector.IO;


import android.media.MediaPlayer;
import android.view.SurfaceView;

import java.util.HashMap;

// Author: Hui Sheng
// Wrapper for playing audio in game
public class AudioManager {

    private SurfaceView view = null;
    private HashMap<Integer, MediaPlayer> audioMap = new HashMap<>();

    public final static AudioManager Instance = new AudioManager();

    private AudioManager() {
    }

    public void Init(SurfaceView _view) {
        view = _view;
        Exit(); // Reset manager just in case
    }

    // Play Audio in Single Channel
    // arg _id: Resource ID
    // arg _volume: Volume percentage from 0 to 1.0
    public void PlayAudio(int _id, float _volume) {

        if (audioMap.containsKey(_id)) {

            MediaPlayer curr = audioMap.get(_id);

            curr.seekTo(0);

            curr.setVolume(_volume, _volume);

            curr.start();

        } else {

            MediaPlayer newAudio = MediaPlayer.create(view.getContext(), _id);
            audioMap.put(_id, newAudio);
            newAudio.start();

        }
    }

    // Clean up the AudioManager before exit
    public void Exit() {
        for (HashMap.Entry<Integer, MediaPlayer> entry : audioMap.entrySet()) {
            entry.getValue().stop();
            entry.getValue().reset();
            entry.getValue().release();
        }
        audioMap.clear();
    }

}
