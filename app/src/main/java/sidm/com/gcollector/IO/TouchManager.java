package sidm.com.gcollector.IO;


import android.view.MotionEvent;

// Author: Hui Sheng
// Responsible for handling touch events in game
public class TouchManager {

    public final static TouchManager Instance = new TouchManager();

    public enum TouchState {
        NONE,
        DOWN,
        MOVE
    }

    private TouchState status = TouchState.NONE;
    private int posX, posY;
    private int deltaPosX, deltaPosY;
    private int offsetX;

    private TouchManager() {
        posX = 0;
        posY = 0;
    }

    // Is there currently a drag event?
    public boolean HasMoved() {
        return status == TouchState.MOVE;
    }

    // Is the screen being touched?
    public boolean HasTouch() {
        return (status == TouchState.DOWN || status == TouchState.MOVE);
    }

    // Touch currently held down?
    public boolean IsDown() {
        return status == TouchState.DOWN;
    }

    public int GetDeltaPosX() {
        return deltaPosX;
    }

    public int GetDeltaPosY() {
        return deltaPosY;
    }

    public int GetPosX() {
        return posX + offsetX;
    }

    public int GetPosY() {
        return posY;
    }

    public void UpdateXOffset(int _offsetX) {
        offsetX = _offsetX;
    }

    public void Update(int _posX, int _posY, int motionEventStatus) {

        deltaPosX = _posX - posX;
        deltaPosY = _posY - posY;

        posX = _posX;
        posY = _posY;


        // Android native
        switch (motionEventStatus) {

            case MotionEvent.ACTION_DOWN:
                status = TouchState.DOWN;
                break;

            case MotionEvent.ACTION_MOVE:
                status = TouchState.MOVE;
                break;

            case MotionEvent.ACTION_UP:
                status = TouchState.NONE;
                break;

        }

    }


}

