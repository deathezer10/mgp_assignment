package sidm.com.gcollector;


import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.view.SurfaceHolder;

import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;
import sidm.com.gcollector.Scene.SceneManager;
import sidm.com.gcollector.View.GameView;


// Author: Hui Sheng
// Worker thread to allow scene to update/render properly
public class UpdateThread extends Thread {

    static final long targetFPS = 60;

    private GameView view = null;
    private SurfaceHolder holder = null;
    private boolean isRunning = false; // Start not running

    public UpdateThread(GameView _view) {
        view = _view;
        holder = _view.getHolder();

        // Init game stuff
        AudioManager.Instance.Init(_view);

        // SceneGamePlay.Instance.Init(_view, this);
        SceneManager.Instance.Init(_view);
        SceneManager.Instance.AddState(SceneGamePlay.Instance);
        SceneManager.Instance.Start("GamePlay");

    }

    public boolean IsRunning() {
        return isRunning;
    }

    public void Initialize() {
        isRunning = true;
    }

    public void Terminate() {
        isRunning = false;
    }

    @Override
    public void run() {

        long framePerSecond = 1000 / targetFPS; // 1000 is in milliseconds
        long startTime = 0;
        long prevTime = System.nanoTime();

        while (IsRunning() && !SceneManager.Instance.GetCurrentState().IsDone()) {

            startTime = System.currentTimeMillis();
            long currTime = System.nanoTime();
            float deltaTime = (float) ((currTime - prevTime) / 1000000000.0f);
            prevTime = currTime;

            // Update
            SceneManager.Instance.Update(deltaTime);

            // Render
            Canvas canvas = holder.lockCanvas(null);

            // Failed to lock canvas
            if (canvas != null) {

                // Prevent 2 threads from rendering at the same time
                synchronized (holder) {
                    // Render whole screen black
                    canvas.drawColor(Color.BLACK);
                    SceneManager.Instance.Render(canvas);
                }

                holder.unlockCanvasAndPost(canvas);
            }

            // Post Update/Render
            try {
                long sleepTime = framePerSecond - (System.currentTimeMillis() - startTime);

                if (sleepTime > 0)
                    sleep(sleepTime);

            } catch (InterruptedException ex) {
                Terminate();
            }
        }

        Log.d("Thread", "Exit 0");
    }

}

