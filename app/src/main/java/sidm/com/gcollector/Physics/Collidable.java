package sidm.com.gcollector.Physics;

// Author: Haziq
// Base class for collision detection
// Implement this if you want to detect collision with other entities
public interface Collidable {

    String GetType();
    float GetPosX();
    float GetPosY();
    float GetRadius();

    void OnHit(Collidable _other);
    void OnTouch();

}
