package sidm.com.gcollector.Physics;

// Author: Hui Sheng
// Vector 2D Class
public class Vec2D {

    public float x, y;

    public boolean IsEqual(Vec2D other) {
        return (other.x == x && other.y == y);
    }

    public Vec2D(float _x, float _y) {
        x = _x;
        y = _y;
    }

    //Set all data
    public void Set(float _x, float _y) {
        x = _x;
        y = _y;
    }

    //Set all data to zero
    public void SetZero() {
        x = 0;
        y = 0;
    }

    //Check if data is zero
    public boolean IsZero() {
        return (x == 0 && y == 0);
    }

    //Vector addition
    public void Add(Vec2D other) {
        x += other.x;
        y += other.y;
    }

    //Vector subtraction
    public void Minus(Vec2D other) {
        x -= other.x;
        y -= other.y;
    }

    // Negate
    public void Negate() {
        x = -x;
        y = -y;
    }

    // Division
    public void Divide(Vec2D other) {
        x /= other.x;
        y /= other.y;
    }

    // Scalar Division
    public void Divide(float scalar) {
        x /= scalar;
        y /= scalar;
    }

    // Scalar multiplication
    public void Mult(float scalar) {
        x *= scalar;
        y *= scalar;
    }

    //Get magnitude
    public float Length() {
        return (float) Math.sqrt((x * x) + (y * y));
    }

    //Get square of magnitude
    public float LengthSquared() {
        return (x * x + y * y);
    }

    //Dot product
    public float Dot(Vec2D other) {
        return ((x * other.x) + (y * other.y));
    }

    //Return a copy of this vector, normalized
    public Vec2D Normalized() {
        Vec2D temp = new Vec2D(x, y);
        temp.Divide(temp.Length());
        return temp;
    }

    //Normalize this vector and return a reference to it
    public void Normalize() {
        Divide(Length());
    }

    // Moves this Vector towards the target with maxDistanceDelta being the rate of movement
    // If next delta distance moved will be greater than target then target will be assigned instead (will not overshoot target)
    // Returns: TRUE if reached the target
    boolean MoveTowards(Vec2D target, float maxDistanceDelta) {
        target.Minus(this);
        Vec2D dir = target;

        float magnitude = dir.LengthSquared();

        if (magnitude <= (maxDistanceDelta * maxDistanceDelta) || magnitude == 0.0f) {
            x = target.x;
            y = target.y;
            return true;
        }

        dir.Divide((float) Math.sqrt(magnitude));
        dir.Mult(maxDistanceDelta);
        Add(dir);

        return false;
    }

}
