package sidm.com.gcollector.Physics;


import java.util.Random;

// Author: Hui Sheng
// Provides various useful Math functions
public class MyMath {

    private MyMath() {
    }

    public static float RandomFloat(float min, float max) {
        Random r = new Random(System.currentTimeMillis());
        return (min + r.nextFloat() * (max - min));
    }

    public static int RandomInt(int min, int max) {
        Random r = new Random(System.currentTimeMillis());
        return (min + r.nextInt() * (max - min));
    }

    public static float Clamp(float value, float min, float max){
        if (value < min)
            return min;
        if (value > max)
            return max;

        return value;
    }

}
