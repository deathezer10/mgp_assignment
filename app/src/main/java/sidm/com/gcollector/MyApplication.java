package sidm.com.gcollector;

import android.app.Application;
import android.app.DialogFragment;
import android.content.Context;


// Author: Hui Sheng
// Used for getting Application's Context in a static way
public class MyApplication extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}

