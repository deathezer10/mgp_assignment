package sidm.com.gcollector.Player;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

import sidm.com.gcollector.Constants.LayerConstants;
import sidm.com.gcollector.Entity.Concrete.Projectile.ProjectileEntity;
import sidm.com.gcollector.Entity.EntityBase;
import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.IO.TouchManager;
import sidm.com.gcollector.Physics.Collidable;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;


// Author: Haziq
// Heart power up, can add more if you want
public class PowerUp implements EntityBase, Collidable {

    private int textureID;

    private Bitmap bmp = null;
    private boolean isDone = false;
    private float xPos, yPos;
    private POWERUP_TYPE type;

    private float yOffset = 0;

    //Types of power up
    //Power up type list
    //1-> life
    //2-> +1 more bullet
    //3-> +2 more bullets
    public enum POWERUP_TYPE {
        POWERUP_LIFE,
        POWERUP_BULLET1,
        POWERUP_BULLET2,
    }

    //Constructor
    public PowerUp(POWERUP_TYPE _type, float x, float y) {
        type = _type;
        xPos = x;
        yPos = y;

        switch (_type) {

            case POWERUP_LIFE:
                textureID = R.drawable.medkit;
                break;

            case POWERUP_BULLET1:
                textureID = R.drawable.bulletup;
                break;

            case POWERUP_BULLET2:
                textureID = R.drawable.woodball;
                break;
        }


    }


    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {
        bmp = BitmapFactory.decodeResource(_view.getResources(), textureID);
    }

    @Override
    public void Update(float _dt) {
        yOffset += _dt;
    }

    @Override
    public void Render(Canvas _canvas) {
        _canvas.drawBitmap(bmp, xPos - bmp.getWidth() * 0.5f, yPos - (50 * (float) Math.cos(yOffset)) - bmp.getHeight() * 0.5f, null);
    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.LAYER_ENTITY;
    }

    //Functions
    @Override
    public String GetType() {
        return "ProjectileEntity";
    }

    @Override
    public float GetPosX() {
        return xPos;
    }

    @Override
    public float GetPosY() {
        return yPos + yOffset;
    }

    @Override
    public float GetRadius() {
        return bmp.getHeight() * 0.75f;
    }

    @Override
    public void OnHit(Collidable _other) {
        if (_other instanceof ProjectileEntity) {

            ProjectileEntity projectile = (ProjectileEntity) _other;
            if (!projectile.isEnemyProjectile) {
                // Player projectile
                switch (type) {
                    case POWERUP_LIFE: // Heal the player on hit
                        AudioManager.Instance.PlayAudio(R.raw.healed, 1);
                        SceneGamePlay.Instance.m_PlayerInfo.IncreaseHealth(40);
                        SetIsDone(true); // destroy the projectile
                        break;

                    case POWERUP_BULLET1:
                        SetIsDone(true); // destroy the projectile
                        break;

                    case POWERUP_BULLET2:
                        SetIsDone(true); // destroy the projectile
                        break;

                }

            }
        }
    }

    @Override
    public void OnTouch() {
        if (!SceneGamePlay.Instance.IsPlayerTurn()) {
            xPos = TouchManager.Instance.GetPosX();
        }
    }

}
