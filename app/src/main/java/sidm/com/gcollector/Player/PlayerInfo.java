package sidm.com.gcollector.Player;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.widget.ProgressBar;

import sidm.com.gcollector.Entity.Concrete.BarrierEntity;
import sidm.com.gcollector.Entity.Concrete.PlayerEntity;
import sidm.com.gcollector.Entity.Concrete.Projectile.ProjectileEntity;
import sidm.com.gcollector.Entity.EntityManager;
import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.IO.TouchManager;
import sidm.com.gcollector.Physics.Collision;
import sidm.com.gcollector.Physics.MyMath;
import sidm.com.gcollector.Physics.Vec2D;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;

// Author: Hui Sheng & Haziq
// Handles Player's Combat mechanics and where to render player sprite entity
public class PlayerInfo {

    public boolean m_UsedShield;
    public boolean m_UsedMultishot;

    float m_CurrentHealth = 0;
    float m_MaxHealth = 0;

    public PlayerEntity m_Player;

    boolean m_Pulling;
    boolean m_WaitForTurnSwitch;
    Vec2D m_InitialPull;
    Vec2D m_FinalPull;
    float m_NextSwitchTurnTime;

    public PlayerInfo(PlayerEntity player) {
        m_Pulling = false;
        m_Player = player;
        m_CurrentHealth = 100;
        m_NextSwitchTurnTime = 0;
        m_WaitForTurnSwitch = false;
        m_UsedShield = false;
        m_UsedMultishot = false;

        // Apply health upgrade if available
        if (SaveData.getInstance().b_Upgrade_Health)
            m_CurrentHealth = 200;

        m_MaxHealth = m_CurrentHealth;
    }

    public void SpawnShield() {
        if (!m_UsedShield) {
            EntityManager.Instance.AddEntity(new BarrierEntity());
            m_UsedShield = true;
        }
    }

    public void SpawnMultiShot() {
        if (!m_UsedMultishot) {

            final float shotVelocity = 1000;
            final double angle1 = Math.toRadians(-60);
            final double angle2 = Math.toRadians(-50);
            final double angle3 = Math.toRadians(-40);

            EntityManager.Instance.AddEntity(new ProjectileEntity(
                    ProjectileEntity.PROJECTILE_TYPE.METAL_BALL,
                    m_Player.GetPosX(),
                    m_Player.GetPosY(),
                    (float) Math.cos(angle1) * shotVelocity,
                    (float) Math.sin(angle1) * shotVelocity,
                    false
            ));

            EntityManager.Instance.AddEntity(new ProjectileEntity(
                    ProjectileEntity.PROJECTILE_TYPE.STONE_BALL,
                    m_Player.GetPosX(),
                    m_Player.GetPosY(),
                    (float) Math.cos(angle2) * shotVelocity,
                    (float) Math.sin(angle2) * shotVelocity,
                    false
            ));

            EntityManager.Instance.AddEntity(new ProjectileEntity(
                    ProjectileEntity.PROJECTILE_TYPE.WOOD_BALL,
                    m_Player.GetPosX(),
                    m_Player.GetPosY(),
                    (float) Math.cos(angle3) * shotVelocity,
                    (float) Math.sin(angle3) * shotVelocity,
                    false
            ));

            m_UsedMultishot = true;
        }
    }

    public float GetCurrentHealth() {
        return m_CurrentHealth;
    }

    public float GetMaxHealth() {
        return m_MaxHealth;
    }

    // Wait X seconds before switching turn
    public void WaitFor(float _timer) {
        m_WaitForTurnSwitch = true;
        m_NextSwitchTurnTime = SceneGamePlay.Instance.m_ElapsedTime + _timer;
    }

    // Is player currently in standby mode?
    public boolean IsWaiting() {
        return m_WaitForTurnSwitch;
    }

    // Current standby timer
    public float GetTurnCountdown() {
        return (m_NextSwitchTurnTime - SceneGamePlay.Instance.m_ElapsedTime);
    }

    public int GetDamage() {
        int damage = 25;

        if (SaveData.getInstance().b_Upgrade_Power)
            damage = 40;

        return damage;
    }

    public void ReduceHealth(float value) {
        m_CurrentHealth -= value;

        if (m_CurrentHealth <= 0) {
            m_CurrentHealth = 0;
            SceneGamePlay.Instance.SetGameOver(true, false);
        }
    }

    public void IncreaseHealth(float value) {
        m_CurrentHealth += value;

        if (m_CurrentHealth > m_MaxHealth) {
            m_CurrentHealth = m_MaxHealth;

        }
    }

    public void Update(float _dt) {

        if (m_WaitForTurnSwitch && SceneGamePlay.Instance.m_ElapsedTime >= m_NextSwitchTurnTime) {
            m_WaitForTurnSwitch = false;
            // Set to enemy's turn
            SceneGamePlay.Instance.SwitchTurn();
        }

        if (!m_WaitForTurnSwitch && SceneGamePlay.Instance.IsPlayerTurn()) {

            // If not pulling slingshot, start pulling now
            if (!m_Pulling &&
                    TouchManager.Instance.IsDown() &&
                    Collision.SphereToSphere(m_Player.GetPosX(), m_Player.GetPosY(), m_Player.GetRadius(), TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY(), 2)) {

                Log.d("Player", "Pulling..");

                m_Pulling = true;

                // Set initial pull position
                m_InitialPull = new Vec2D(m_Player.GetPosX(), m_Player.GetPosY());

            }

            if (m_Pulling) {

                // Player let go of the slingshot, fire away!
                if (!TouchManager.Instance.HasTouch()) {
                    m_Pulling = false;

                    // Set initial pull position
                    m_FinalPull = new Vec2D(TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY());

                    m_InitialPull.Minus(m_FinalPull);

                    if (!m_InitialPull.IsZero()) {

                        float Speed = m_InitialPull.Length() * 5;
                        m_InitialPull.Normalize();

                        // Limit the speed
                        if (Speed > 2000)
                            Speed = 2000;

                        m_InitialPull.Mult(Speed);

                        AudioManager.Instance.PlayAudio(R.raw.launch, 1); // play audio

                        EntityManager.Instance.AddEntity(new ProjectileEntity(
                                ProjectileEntity.PROJECTILE_TYPE.PAPER_BALL,
                                m_Player.GetPosX(),
                                m_Player.GetPosY(),
                                m_InitialPull.x,
                                m_InitialPull.y,
                                false
                        ));

                        WaitFor(4);

                        Log.d("Player", "Fired! Speed: " + String.valueOf(Speed));
                    }
                }

            }

        }

    }

    public void Render(Canvas _canvas) {

        ProgressBar bar = SceneGamePlay.Instance.m_Context.findViewById(R.id.playerhp);
        bar.setProgress((int) ((m_CurrentHealth / m_MaxHealth) * 100));

        // Draw Pulling Line onto canvas
        if (m_Pulling) {

            // Line Style
            Paint paint = new Paint();
            paint.setColor(Color.parseColor("#FD8535"));
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(8); // Line width in pixels
            paint.setAntiAlias(true);

            Vec2D startPoint = new Vec2D(
                    TouchManager.Instance.GetPosX(), // startX
                    TouchManager.Instance.GetPosY() // startY
            );

            Vec2D endPoint = new Vec2D(
                    m_Player.GetPosX() + (m_Player.GetPosX() - TouchManager.Instance.GetPosX()),// stopX
                    m_Player.GetPosY() + (m_Player.GetPosY() - TouchManager.Instance.GetPosY()) // stopY
            );

            _canvas.drawLine(
                    startPoint.x, // startX
                    startPoint.y, // startY
                    endPoint.x,
                    endPoint.y,
                    paint // Paint
            );

        }

    }

}