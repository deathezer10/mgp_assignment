package sidm.com.gcollector.Player;


import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Collections;

import sidm.com.gcollector.MyApplication;


// Author: Hui Sheng
// Provides methods to Save and Load player's data from the device
public class SaveData {

    // Level Unlocked Data
    public boolean b_Level_1;
    public boolean b_Level_2;
    public boolean b_Level_3;

    // Upgrades Data
    public boolean b_Upgrade_Health;
    public boolean b_Upgrade_Power;
    public boolean b_Upgrade_Parry;
    public boolean b_Upgrade_Bottle;
    public boolean b_Upgrade_Regen;

    // High Score Data
    public long m_Highscore_1;
    public long m_Highscore_2;
    public long m_Highscore_3;
    public long m_Highscore_4;
    public long m_Highscore_5;

    // Singleton
    private static final SaveData ourInstance = new SaveData();

    public static SaveData getInstance() {
        return ourInstance;
    }

    private SaveData() {

        SharedPreferences pref = MyApplication.getAppContext().getSharedPreferences("SaveData", 0);

        // Level unlock
        b_Level_1 = pref.getBoolean("Level1", false);
        b_Level_2 = pref.getBoolean("Level2", false);
        b_Level_3 = pref.getBoolean("Level3", false);

        // Upgrades Data
        b_Upgrade_Health = pref.getBoolean("Upgrade_Health", false);
        b_Upgrade_Power = pref.getBoolean("Upgrade_Power", false);
        b_Upgrade_Parry = pref.getBoolean("Upgrade_Parry", false);
        b_Upgrade_Bottle = pref.getBoolean("Upgrade_Bottle", false);
        b_Upgrade_Regen = pref.getBoolean("Upgrade_Regen", false);

        // High Score Data
        m_Highscore_1 = pref.getLong("HScore_1", 0);
        m_Highscore_2 = pref.getLong("HScore_2", 0);
        m_Highscore_3 = pref.getLong("HScore_3", 0);
        m_Highscore_4 = pref.getLong("HScore_4", 0);
        m_Highscore_5 = pref.getLong("HScore_5", 0);

    }

    // Saves all current player data onto the device
    public void Save() {

        SharedPreferences pref = MyApplication.getAppContext().getSharedPreferences("SaveData", 0);
        SharedPreferences.Editor editor = pref.edit();

        // Level unlock
        editor.putBoolean("Level1", b_Level_1);
        editor.putBoolean("Level2", b_Level_2);
        editor.putBoolean("Level3", b_Level_3);

        // Upgrades Data
        editor.putBoolean("Upgrade_Health", b_Upgrade_Health);
        editor.putBoolean("Upgrade_Power", b_Upgrade_Power);
        editor.putBoolean("Upgrade_Parry", b_Upgrade_Parry);
        editor.putBoolean("Upgrade_Bottle", b_Upgrade_Bottle);
        editor.putBoolean("Upgrade_Regen", b_Upgrade_Regen);

        // High Score Data
        editor.putLong("HScore_1", m_Highscore_1);
        editor.putLong("HScore_2", m_Highscore_2);
        editor.putLong("HScore_3", m_Highscore_3);
        editor.putLong("HScore_4", m_Highscore_4);
        editor.putLong("HScore_5", m_Highscore_5);

        editor.commit();

    }

    // Are all upgrades unlocked?
    public boolean IsFullyUpgraded() {
        return (b_Upgrade_Bottle && b_Upgrade_Health && b_Upgrade_Parry && b_Upgrade_Power && b_Upgrade_Regen);
    }

    // Maxes all stats upgrade and save it
    public void MaxEverything() {

        b_Level_1 = true;
        b_Level_2 = true;
        b_Level_3 = true;

        // Upgrades Data
        b_Upgrade_Health = true;
        b_Upgrade_Power = true;
        b_Upgrade_Parry = true;
        b_Upgrade_Bottle = true;
        b_Upgrade_Regen = true;

        // High Score Data
        m_Highscore_1 = 5145;
        m_Highscore_2 = 4121;
        m_Highscore_3 = 342;
        m_Highscore_4 = 55;
        m_Highscore_5 = 18;

        Save();
    }

    // Reset all existing save data, returning them to their default state
    public void ResetEverything() {

        b_Level_1 = false;
        b_Level_2 = false;
        b_Level_3 = false;

        // Upgrades Data
        b_Upgrade_Health = false;
        b_Upgrade_Power = false;
        b_Upgrade_Parry = false;
        b_Upgrade_Bottle = false;
        b_Upgrade_Regen = false;

        // High Score Data
        m_Highscore_1 = 0;
        m_Highscore_2 = 0;
        m_Highscore_3 = 0;
        m_Highscore_4 = 0;
        m_Highscore_5 = 0;

        Save();
    }

    // Returns the upgrade that hasn't been upgraded, 0 if all are upgraded
    public int GetUniqueReward() {
        if (!b_Upgrade_Health)
            return 1;
        if (!b_Upgrade_Power)
            return 2;
        if (!b_Upgrade_Parry)
            return 3;
        if (!b_Upgrade_Bottle)
            return 4;
        if (!b_Upgrade_Regen)
            return 5;

        return 0;
    }

    public void AddHighScore(long score) {

        // Naive way of sorting lol
        ArrayList<Long> list = new ArrayList<>();

        list.add(m_Highscore_1);
        list.add(m_Highscore_2);
        list.add(m_Highscore_3);
        list.add(m_Highscore_4);
        list.add(m_Highscore_5);
        list.add(score);

        // Sort in Descending order
        Collections.sort(list, Collections.reverseOrder());

        // Replace the highest number if possible
        m_Highscore_1 = list.get(0);
        m_Highscore_2 = list.get(1);
        m_Highscore_3 = list.get(2);
        m_Highscore_4 = list.get(3);
        m_Highscore_5 = list.get(4);

        Save();
    }

}
