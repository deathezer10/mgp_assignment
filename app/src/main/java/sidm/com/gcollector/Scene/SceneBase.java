package sidm.com.gcollector.Scene;


import android.graphics.Canvas;
import android.view.SurfaceView;


// Author: Hui Sheng
// Base class for every Scenes
public interface SceneBase {

    void OnEnter(SurfaceView _view);
    void OnExit();

    void Update(float _dt);
    void Render(Canvas _canvas);

    String GetName();
    boolean IsDone();

}
