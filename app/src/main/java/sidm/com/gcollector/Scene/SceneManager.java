package sidm.com.gcollector.Scene;

import android.graphics.Canvas;
import android.view.SurfaceView;

import java.util.HashMap;


// Author: Hui Sheng
// Provides functions to change the currently loaded scene
public class SceneManager {

    // Singleton Instance
    public static final SceneManager Instance = new SceneManager();

    private SurfaceView view = null;

    // State container
    private HashMap<String, SceneBase> stateMap = new HashMap<>();

    private SceneBase currState = null;
    private SceneBase nextState = null;

    private SceneManager() {
    }

    public void Init(SurfaceView _view) {
        view = _view;
    }

    public void Update(float _dt) {

        // Process all state changes
        if (nextState != currState) {
            currState.OnExit();
            nextState.OnEnter(view);
            currState = nextState;
        }

        // Safety Check
        if (currState == null)
            return;

        currState.Update(_dt);
    }

    public void Render(Canvas _canvas) {
        currState.Render(_canvas);
    }

    public void ChangeState(String _nextState) {
        // Try to get our next state
        nextState = stateMap.get(_nextState);

        // What if there's no next state?
        if (nextState == null)
            nextState = currState;
    }

    public void AddState(SceneBase _newState) {
        stateMap.put(_newState.GetName(), _newState);
    }

    public void Start(String _newCurrent) {

        if (currState != null)
            currState.OnExit();

        currState = stateMap.get(_newCurrent);

        if (currState != null) {
            currState.OnEnter(view);
            nextState = currState;
        }
    }

    public SceneBase GetCurrentState() {
        return currState;
    }

}

