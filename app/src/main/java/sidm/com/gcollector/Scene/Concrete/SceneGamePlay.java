package sidm.com.gcollector.Scene.Concrete;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import sidm.com.gcollector.Entity.Concrete.BackgroundEntity;
import sidm.com.gcollector.Entity.Concrete.Enemy.EnemyBar;
import sidm.com.gcollector.Entity.Concrete.Enemy.EnemyBaz;
import sidm.com.gcollector.Entity.Concrete.Enemy.EnemyFoo;
import sidm.com.gcollector.Entity.Concrete.PlayerEntity;
import sidm.com.gcollector.Entity.EnemyBase;
import sidm.com.gcollector.Entity.EntityBase;
import sidm.com.gcollector.Entity.EntityManager;
import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.Physics.MyMath;
import sidm.com.gcollector.Player.PlayerInfo;
import sidm.com.gcollector.Player.PowerUp;
import sidm.com.gcollector.Player.SaveData;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.SceneBase;

// Author: Hui Sheng &  Haziq
// Main game play scene, determines what entities are added to a scene and handles win/lose conditions
public class SceneGamePlay implements SceneBase {

    public final static SceneGamePlay Instance = new SceneGamePlay();
    public Activity m_Context;
    public boolean IsScrollable = false;
    public PlayerInfo m_PlayerInfo;
    public int m_CurrentLevel;
    public boolean m_IsGamePaused;
    public float m_ElapsedTime;

    // For Scrolling of elements
    public int m_CanvasOffsetX;

    // UI Elements
    private TextView lblCurrentTurn;

    // Gameplay Helper Variables
    private boolean m_IsDone;
    private boolean m_Victory;
    private boolean m_GameOver;
    private boolean m_IsPlayerTurn;
    private EnemyBase m_CurrentEnemy;
    public boolean dialogState = false;

    // Stop the UpdateThread
    public void Terminate() {
        m_IsDone = true;
    }

    public void SetGameOver(boolean _gameOver, boolean _victory) {
        m_GameOver = _gameOver;
        m_Victory = _victory;

        if (m_Victory) {
            // Set current score and save it
            long currentScore = (long) (100000 / m_ElapsedTime);

            ((TextView) m_Context.findViewById(R.id.lblGameplayCurrentScore)).setText(Long.toString(currentScore));

            SaveData.getInstance().AddHighScore(currentScore);
        }
    }

    public void SwitchTurn() {
        m_IsPlayerTurn = !m_IsPlayerTurn;

        if (m_IsPlayerTurn && SaveData.getInstance().b_Upgrade_Regen && m_PlayerInfo.GetCurrentHealth() != m_PlayerInfo.GetMaxHealth()) {
            AudioManager.Instance.PlayAudio(R.raw.healed, 1);
            m_PlayerInfo.IncreaseHealth(40);
        }

    }

    public boolean IsVictory() {
        return m_Victory;
    }

    public boolean IsGameOver() {
        return m_GameOver;
    }

    public boolean IsPlayerTurn() {
        return m_IsPlayerTurn;
    }

    private SceneGamePlay() {
    }

    @Override
    public void OnEnter(SurfaceView _view) {

        m_IsDone = false;
        IsScrollable = false;
        m_CanvasOffsetX = 0;
        m_ElapsedTime = 0;
        m_IsPlayerTurn = true;
        m_Victory = false;
        m_GameOver = false;
        m_IsGamePaused = false;

        // Init player
        PlayerEntity player = new PlayerEntity();
        m_PlayerInfo = new PlayerInfo(player);

        EntityManager.Instance.Reset();
        EntityManager.Instance.Init(_view);

        // Initial Entities
        // EntityManager.Instance.AddEntity(new FPSEntity()); // Show FPS entity
        EntityManager.Instance.AddEntity(new BackgroundEntity());
        EntityManager.Instance.AddEntity(player);

        // Spawn enemy depending on current level
        EnemyBase enemy = null;

        switch (m_CurrentLevel) {
            case 0:
                enemy = new EnemyBar();
                AudioManager.Instance.PlayAudio(R.raw.timeattack, 0.5f);
                dialogState = false;
                break;

            case 1:
                enemy = new EnemyFoo();
                AudioManager.Instance.PlayAudio(R.raw.timeattack, 0.5f);
                dialogState = false;
                break;

            case 2:
                enemy = new EnemyBaz();
                AudioManager.Instance.PlayAudio(R.raw.gravitylord, 0.75f);
                dialogState = true;
                break;
        }

        // Spawn power up
        EntityManager.Instance.AddEntity(new PowerUp(PowerUp.POWERUP_TYPE.POWERUP_LIFE, MyMath.RandomFloat(800, 1000), (int) MyMath.RandomFloat(300, 500)));

        m_CurrentEnemy = enemy;
        EntityManager.Instance.AddEntity((EntityBase) m_CurrentEnemy);

    }

    @Override
    public void OnExit() {

    }

    public void Update(float _dt) {

        // Update GUI
        m_Context.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (m_IsGamePaused) {
                    m_Context.findViewById(R.id.pausepanel).setVisibility(LinearLayout.VISIBLE);
                    return;
                } else {
                    m_Context.findViewById(R.id.pausepanel).setVisibility(LinearLayout.GONE);
                }

                if (m_GameOver) {

                    if (m_Victory) { // Player win
                        m_Context.findViewById(R.id.winpanel).setVisibility(LinearLayout.VISIBLE);

                        if (m_CurrentLevel == 2)
                            m_Context.findViewById(R.id.btnGoNext).setVisibility(Button.INVISIBLE);

                    } else // Player defeated
                        m_Context.findViewById(R.id.losepanel).setVisibility(LinearLayout.VISIBLE);

                }

                if (m_CurrentLevel == 2 && dialogState ){
                    RelativeLayout dialog2 = m_Context.findViewById(R.id.DialogPanel);
                    dialog2.setVisibility(View.VISIBLE);
                }else{
                    RelativeLayout dialog2 = m_Context.findViewById(R.id.DialogPanel);
                    dialog2.setVisibility(View.GONE);
                }

                // Display multishot/parry during player's turn
                if (!IsPlayerTurn()) {
                    if (!m_PlayerInfo.m_UsedShield && SaveData.getInstance().b_Upgrade_Parry)
                        m_Context.findViewById(R.id.btnParry).setVisibility(View.VISIBLE);
                } else {
                    m_Context.findViewById(R.id.btnParry).setVisibility(View.INVISIBLE);

                    if (!m_PlayerInfo.m_UsedMultishot && SaveData.getInstance().b_Upgrade_Bottle && !m_PlayerInfo.IsWaiting())
                        m_Context.findViewById(R.id.btnMultishot).setVisibility(View.VISIBLE);
                    else
                        m_Context.findViewById(R.id.btnMultishot).setVisibility(View.INVISIBLE);
                }

                lblCurrentTurn = m_Context.findViewById(R.id.lblCurrentTurn);

                if (m_PlayerInfo.IsWaiting()) {
                    lblCurrentTurn.setTextColor(Color.WHITE);
                    lblCurrentTurn.setText(m_Context.getResources().getString(R.string.game_standby, (int) (m_PlayerInfo.GetTurnCountdown() + 1)));
                    lblCurrentTurn.setTypeface(null, Typeface.ITALIC);
                } else if (m_IsPlayerTurn) {
                    lblCurrentTurn.setTextColor(Color.GREEN);
                    lblCurrentTurn.setText(R.string.game_playerturn);
                } else {
                    lblCurrentTurn.setTextColor(Color.RED);
                    lblCurrentTurn.setText(R.string.game_enemyturn);
                }
            }
        });

        // Stop updating game instance if game over or paused
        if (m_GameOver || m_IsGamePaused)
            return;

        m_ElapsedTime += _dt;

        EntityManager.Instance.Update(_dt);

        m_PlayerInfo.Update(_dt);

        // If Enemy Dies, game win!
        if (m_CurrentEnemy.GetCurrentHP() <= 0) {
            SetGameOver(true, true);

            // Unlock next level
            if (m_CurrentLevel == 0)
                SaveData.getInstance().b_Level_2 = true;
            else if (m_CurrentLevel == 1)
                SaveData.getInstance().b_Level_3 = true;

            // Give reward to player
            int rewardID = SaveData.getInstance().GetUniqueReward();
            int iconID;
            int stringID;

            switch (rewardID) {
                case 1:
                    stringID = R.string.reward_health;
                    iconID = R.drawable.healthupgrade;
                    SaveData.getInstance().b_Upgrade_Health = true;
                    break;

                case 2:
                    stringID = R.string.reward_attack;
                    iconID = R.drawable.attackupgrade;
                    SaveData.getInstance().b_Upgrade_Power = true;
                    break;

                case 3:
                    stringID = R.string.reward_parry;
                    iconID = R.drawable.parryupgrade;
                    SaveData.getInstance().b_Upgrade_Parry = true;
                    break;

                case 4:
                    stringID = R.string.reward_multishot;
                    iconID = R.drawable.multishotupgrade;
                    SaveData.getInstance().b_Upgrade_Bottle = true;
                    break;

                case 5:
                    stringID = R.string.reward_regen;
                    iconID = R.drawable.regenupgrade;
                    SaveData.getInstance().b_Upgrade_Regen = true;
                    break;

                default:
                    stringID = R.string.reward_none;
                    iconID = R.drawable.maxedupgrade;
                    break;

            }

            // Assign the IDs to the text view and img view
            ((TextView) m_Context.findViewById(R.id.lblRewardName)).setText(stringID);
            ((ImageView) m_Context.findViewById(R.id.imgRewardIcon)).setImageResource(iconID);

            // Save data
            SaveData.getInstance().Save();
        }

    }

    public void Render(Canvas _canvas) {
        _canvas.translate(m_CanvasOffsetX, 0);
        EntityManager.Instance.Render(_canvas);

        m_PlayerInfo.Render(_canvas);
    }

    @Override
    public String GetName() {
        return "GamePlay";
    }

    @Override
    public boolean IsDone() {
        return m_IsDone;
    }

}

