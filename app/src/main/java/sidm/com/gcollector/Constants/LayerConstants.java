package sidm.com.gcollector.Constants;


// Author: Hui Sheng
// Constants to determine the render order for all entities
public class LayerConstants {

    public final static int LAYER_BACKGROUND = 0;
    public final static int LAYER_ENTITY_BG = 1;
    public final static int LAYER_ENTITY = 2;
    public final static int LAYER_PROJECTILE = 3;
    public final static int LAYER_UI = 4;

}
