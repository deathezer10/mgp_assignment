package sidm.com.gcollector.Entity;


import android.graphics.Canvas;
import android.view.SurfaceView;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;

import sidm.com.gcollector.IO.TouchManager;
import sidm.com.gcollector.Physics.Collidable;
import sidm.com.gcollector.Physics.Collision;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;


// Author: Hui Sheng
// Handles adding and removing of game entities
public class EntityManager {

    public static EntityManager Instance = new EntityManager();
    private SurfaceView view = null;
    private LinkedList<EntityBase> entityList = new LinkedList<>();

    // Queue for Initing Entities
    private Queue<EntityBase> entityInitQueue = new LinkedList<>();

    private EntityManager() {
    }

    public void Init(SurfaceView _view) {
        view = _view;
    }

    public void Update(float _dt) {

        // Initialize all pending Entities
        while (entityInitQueue.isEmpty() == false) {
            EntityBase newEntity = entityInitQueue.remove();
            newEntity.Init(view);
            entityList.add(newEntity);
        }

        LinkedList<EntityBase> removalList = new LinkedList<>();

        for (EntityBase currEntity : entityList) {
            currEntity.Update(_dt);

            if (currEntity.IsDone()) {
                removalList.add(currEntity);
            }
        }

        entityList.removeAll(removalList);
        removalList.clear();

        SceneGamePlay.Instance.IsScrollable = true;

        for (int i = 0; i < entityList.size(); ++i) {
            EntityBase currEntity = entityList.get(i);

            if (currEntity instanceof Collidable) {
                Collidable first = (Collidable) currEntity;

                // Check OnTouch first
                if (TouchManager.Instance.HasTouch() && Collision.SphereToSphere(first.GetPosX(), first.GetPosY(), first.GetRadius(), TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY(), 2)) {
                    SceneGamePlay.Instance.IsScrollable = false;
                    first.OnTouch();
                }

                for (int j = i + 1; j < entityList.size(); ++j) {
                    EntityBase otherEntity = entityList.get(j);

                    if (otherEntity instanceof Collidable) {
                        Collidable second = (Collidable) otherEntity;

                        // We got our 2 collidable entities, check collision here
                        if (Collision.SphereToSphere(first.GetPosX(), first.GetPosY(), first.GetRadius(), second.GetPosX(), second.GetPosY(), second.GetRadius())) {
                            first.OnHit(second);
                            second.OnHit(first);
                        }
                    }
                }
            }

            if (currEntity.IsDone()) {
                removalList.add(currEntity);
            }
        }

        entityList.removeAll(removalList);
        removalList.clear();
    }

    public void Render(Canvas _canvas) {

        // Sort the list with its current layer
        Collections.sort(entityList, new Comparator<EntityBase>() {
            @Override
            public int compare(EntityBase o1, EntityBase o2) {
                return o1.GetRenderLayer() - o2.GetRenderLayer();
            }
        });

        for (EntityBase currEntity : entityList) {
            currEntity.Render(_canvas);
        }

    }

    public void AddEntity(EntityBase _newEntity) {
        entityInitQueue.add(_newEntity);
    }

    // Reset this Instance in case the User pressed back button
    public void Reset() {
        entityList.clear();
        Instance = new EntityManager();
    }
}


