package sidm.com.gcollector.Entity.Concrete;


import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.SurfaceView;

import sidm.com.gcollector.Constants.LayerConstants;
import sidm.com.gcollector.Entity.EntityBase;

// Author: Haziq
// Render FPS text on screen
public class FPSEntity implements EntityBase {

    boolean isDone = false;
    float m_CurrentFPS;
    SurfaceView view;

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {
        view = _view;
    }

    @Override
    public void Update(float _dt) {
        m_CurrentFPS = 1.f / _dt;
    }

    @Override
    public void Render(Canvas _canvas) {

        Paint paint = new Paint();
        paint.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), "font/Gemcut.otf"));
        paint.setARGB(255, 255, 255, 255);
        paint.setStrokeWidth(100);
        paint.setTextSize(50);
        float twoDP = Math.round(m_CurrentFPS * 100.f) / 100.f;
        _canvas.drawText("FPS: " + (int) twoDP, 50, 75, paint);

    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.LAYER_UI;
    }
}
