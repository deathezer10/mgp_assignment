package sidm.com.gcollector.Entity.Concrete.Projectile;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

import sidm.com.gcollector.Constants.LayerConstants;
import sidm.com.gcollector.Entity.EntityBase;
import sidm.com.gcollector.Physics.Collidable;
import sidm.com.gcollector.R;

public class ProjectileEntity implements EntityBase, Collidable {

    public boolean isEnemyProjectile;

    private Bitmap bmp = null;
    private boolean isDone = false;
    private float xPos, yPos, xDir, yDir, lifeTime;
    private int textureID;

    public enum PROJECTILE_TYPE {
        PAPER_BALL,
        METAL_BALL,
        WOOD_BALL,
        STONE_BALL,
        ROCKET_SPIKE
    }

    public ProjectileEntity(PROJECTILE_TYPE _type, float _xPos, float _yPos, float _xDir, float _yDir, boolean _isEnemyProjectile) {

        isEnemyProjectile = _isEnemyProjectile;

        switch (_type) {

            case PAPER_BALL:
                textureID = R.drawable.paperball;
                break;

            case METAL_BALL:
                textureID = R.drawable.metalball;
                break;

            case WOOD_BALL:
                textureID = R.drawable.woodball;
                break;

            case STONE_BALL:
                textureID = R.drawable.stoneball;
                break;

            case ROCKET_SPIKE:
                textureID = R.drawable.rocket_spike;
                break;

        }

        xPos = _xPos;
        yPos = _yPos;

        xDir = _xDir;
        yDir = _yDir;

        lifeTime = 7;

    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {
        bmp = BitmapFactory.decodeResource(_view.getResources(), textureID);
    }

    @Override
    public void Update(float _dt) {

        lifeTime -= _dt;

        // Prevent projectile from flying forever
        if (lifeTime <= 0)
            SetIsDone(true);

        xPos += xDir * _dt;
        yPos += yDir * _dt;

        // Apply gravity
        yDir += 980 * _dt;

    }

    @Override
    public void Render(Canvas _canvas) {

        _canvas.drawBitmap(bmp, xPos - bmp.getWidth() * 0.5f, yPos - bmp.getHeight() * 0.5f, null);

    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.LAYER_PROJECTILE;
    }

    @Override
    public String GetType() {
        return "ProjectileEntity";
    }

    @Override
    public float GetPosX() {
        return xPos;
    }

    @Override
    public float GetPosY() {
        return yPos;
    }

    @Override
    public float GetRadius() {
        return ((bmp.getWidth() > bmp.getHeight()) ? bmp.getWidth() : bmp.getHeight()) * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other) {
    }

    @Override
    public void OnTouch() {
    }
}
