package sidm.com.gcollector.Entity.Concrete;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

import sidm.com.gcollector.Entity.Concrete.Projectile.ProjectileEntity;
import sidm.com.gcollector.Constants.LayerConstants;
import sidm.com.gcollector.Entity.EntityBase;
import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.Physics.Collidable;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;

// Author: Hui Sheng
// Barrier collision and texture, removes any projectile entities that collided with this
public class BarrierEntity implements EntityBase, Collidable {

    private Bitmap[] bmp = new Bitmap[4];
    private int m_SpriteIndex = 0;
    private float m_SpriteTimer = 0;
    private boolean m_SpriteReverse;

    private boolean isDone = false;
    private float xPos, yPos, lifeTime;

    public BarrierEntity() {
    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {
        bmp[0] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.barrier1);
        bmp[1] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.barrier2);
        bmp[2] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.barrier3);
        bmp[3] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.barrier4);
        yPos = _view.getHeight() - (bmp[0].getHeight() / 2);

        m_SpriteReverse = false;

        lifeTime = 5;
    }

    @Override
    public void Update(float _dt) {

        lifeTime -= _dt;

        if (lifeTime <= 0){
            SetIsDone(true);
            return;
        }

        if (m_SpriteReverse)
            m_SpriteTimer -= _dt * 15; // Sprite animation speed
        else
            m_SpriteTimer += _dt * 15;

        if (m_SpriteTimer <= 0 || m_SpriteTimer >= 3) {
            m_SpriteReverse = !m_SpriteReverse;
        }

        if (m_SpriteTimer < 0)
            m_SpriteTimer = 0;

        if (m_SpriteTimer > 3)
            m_SpriteTimer = 3;

        m_SpriteIndex = (int) (m_SpriteTimer % 4); // modulo by max sprite count

    }

    @Override
    public void Render(Canvas _canvas) {
        xPos = SceneGamePlay.Instance.m_PlayerInfo.m_Player.GetPosX();
        yPos = SceneGamePlay.Instance.m_PlayerInfo.m_Player.GetPosY();

        _canvas.drawBitmap(bmp[m_SpriteIndex], xPos - bmp[m_SpriteIndex].getWidth() * 0.5f, yPos - bmp[m_SpriteIndex].getHeight() * 0.5f, null);
    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.LAYER_ENTITY_BG;
    }

    @Override
    public String GetType() {
        return "BarrierEntity";
    }

    @Override
    public float GetPosX() {
        return xPos;
    }

    @Override
    public float GetPosY() {
        return yPos;
    }

    @Override
    public float GetRadius() {
        return ((bmp[m_SpriteIndex].getWidth() > bmp[m_SpriteIndex].getHeight()) ? bmp[m_SpriteIndex].getWidth() : bmp[m_SpriteIndex].getHeight()) * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other) {
        if (_other instanceof ProjectileEntity) {

            ProjectileEntity projectile = (ProjectileEntity) _other;

            // Enemy hit player
            if (projectile.isEnemyProjectile) {
                projectile.SetIsDone(true);
                AudioManager.Instance.PlayAudio(R.raw.barrierhit, 1);
            }
        }
    }

    @Override
    public void OnTouch() {
    }
}
