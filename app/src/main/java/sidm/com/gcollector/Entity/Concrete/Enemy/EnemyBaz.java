package sidm.com.gcollector.Entity.Concrete.Enemy;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;
import android.widget.ProgressBar;

import java.util.Random;

import sidm.com.gcollector.Entity.Concrete.Projectile.ProjectileEntity;
import sidm.com.gcollector.Constants.LayerConstants;
import sidm.com.gcollector.Entity.EnemyBase;
import sidm.com.gcollector.Entity.EntityBase;
import sidm.com.gcollector.Entity.EntityManager;
import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.IO.TouchManager;
import sidm.com.gcollector.Physics.Collidable;
import sidm.com.gcollector.Physics.MyMath;
import sidm.com.gcollector.Physics.Vec2D;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;

// Author: Haziq
// Enemy Petrifighter: Throws wooden balls or fire missiles at the player
public class EnemyBaz implements EntityBase, Collidable, EnemyBase {

    float m_CurrentHealth = 100;

    private Bitmap[] bmp = new Bitmap[4];

    private boolean isDone = false;
    private float xPos, yPos, xDir, yDir, lifeTime;
    private int m_SpriteIndex = 0;
    private float m_SpriteTimer = 0;


    // Helpers
    final float m_ShootInterval = 2;
    float m_NextShootTime;
    int m_AmmoLeft;
    int xDistance = 0;

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {
        bmp[0] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.enemy3);
        bmp[1] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.enemy32);
        bmp[2] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.enemy33);
        bmp[3] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.enemy34);

        //bmp = Bitmap.createScaledBitmap(bmp, _view.getWidth() / 4, _view.getHeight() / 3, false); // Resize to fit screen

        xPos = _view.getWidth() * 0.85f;
        yPos = _view.getHeight() * 0.7f;
    }

    @Override
    public void Update(float _dt) {// Enemy's turn time to attack!
        if (!SceneGamePlay.Instance.IsPlayerTurn() && !SceneGamePlay.Instance.m_PlayerInfo.IsWaiting()) {
            m_SpriteTimer += _dt * 8; // Sprite animation speed
            m_SpriteIndex = (int) (m_SpriteTimer % 4); // modulo by max sprite count

            if (SceneGamePlay.Instance.m_ElapsedTime >= m_NextShootTime) {

                // Add cooldown
                m_NextShootTime = SceneGamePlay.Instance.m_ElapsedTime + m_ShootInterval;

                // Random direction
                Random rand = new Random();
                float rad = MyMath.RandomFloat(1.57f, 3.14f); // Random 90 to 180 degrees in radians
                final float speed = 1000;
                Vec2D direction = new Vec2D((float) Math.cos(rad), (float) Math.sin(rad));
                Vec2D direction2 = new Vec2D((float) Math.cos(1.57f), (float) Math.sin(1.57f));
                direction.Mult(speed);

                // Play audio


                int random = (int) MyMath.RandomFloat(0, 1);
                int value = rand.nextInt(3);

                switch (value) {
                    case 0:
                        // Spawn projectile
                        AudioManager.Instance.PlayAudio(R.raw.toss, 1);
                        EntityManager.Instance.AddEntity(new ProjectileEntity(
                                ProjectileEntity.PROJECTILE_TYPE.WOOD_BALL,
                                xPos,
                                yPos,
                                direction.x,
                                -direction.y,
                                true
                        ));
                        break;
                    case 1:
                        // Spawn projectile
                        AudioManager.Instance.PlayAudio(R.raw.missile, 1);
                        EntityManager.Instance.AddEntity(new ProjectileEntity(
                                ProjectileEntity.PROJECTILE_TYPE.ROCKET_SPIKE,
                                SceneGamePlay.Instance.m_PlayerInfo.m_Player.GetPosX(),
                                -yPos,
                                direction2.x,
                                direction2.y,
                                true
                        ));
                        break;
                    case 2:
                        AudioManager.Instance.PlayAudio(R.raw.toss, 1);
                        // Spawn projectile
                        EntityManager.Instance.AddEntity(new ProjectileEntity(
                                ProjectileEntity.PROJECTILE_TYPE.WOOD_BALL,
                                xPos + 50 + (int) (Math.random() * ((200 - 50) + 1)),
                                yPos + 50 + (int) (Math.random() * ((200 - 50) + 1)),
                                direction.x + 5 + (int) (Math.random() * ((200 - 50) + 1)),
                                -direction.y + 5 + (int) (Math.random() * ((200 - 50) + 1)),
                                true

                        ));
                        EntityManager.Instance.AddEntity(new ProjectileEntity(
                                ProjectileEntity.PROJECTILE_TYPE.WOOD_BALL,
                                xPos + 5 + (int) (Math.random() * ((200 - 50) + 1)),
                                yPos + 5 + (int) (Math.random() * ((200 - 50) + 1)),
                                direction.x + 5 + (int) (Math.random() * ((200 - 50) + 1)),
                                -direction.y + 5 + (int) (Math.random() * ((200 - 50) + 1)),
                                true

                        ));
                        EntityManager.Instance.AddEntity(new ProjectileEntity(
                                ProjectileEntity.PROJECTILE_TYPE.WOOD_BALL,
                                xPos + 50 + (int) (Math.random() * ((200 - 50) + 1)),
                                yPos + 50 + (int) (Math.random() * ((200 - 50) + 1)),
                                direction.x + 5 + (int) (Math.random() * ((200 - 50) + 1)),
                                -direction.y + 5 + (int) (Math.random() * ((200 - 50) + 1)),
                                true

                        ));
                        break;

                }

                /*EntityManager.Instance.AddEntity(new ProjectileEntity(
                        ProjectileEntity.PROJECTILE_TYPE.ROCKET_SPIKE,
                        xPos -300 +xDistance - (50 + (int)(Math.random() * ((500 - 50) + 1))),
                        yPos,
                        direction.x,
                        -direction.y
                ));*/
                m_AmmoLeft--;
                xDistance -= 400;

                if (m_AmmoLeft <= 0) {
                    SceneGamePlay.Instance.m_PlayerInfo.WaitFor(4);
                    m_SpriteIndex = 0; // reset sprite index when not attacking
                    xDistance += 1200;
                }

            }

        } else {
            m_AmmoLeft = 3;
        }

    }

    @Override
    public void Render(Canvas _canvas) {

        // Update Progress Bar
        ProgressBar bar = SceneGamePlay.Instance.m_Context.findViewById(R.id.enemyhp);
        bar.setProgress((int) m_CurrentHealth);

        _canvas.drawBitmap(bmp[m_SpriteIndex], xPos - bmp[m_SpriteIndex].getWidth() * 0.5f, yPos - bmp[m_SpriteIndex].getHeight() * 0.5f, null);


    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.LAYER_ENTITY;
    }

    @Override
    public String GetType() {
        return "BazEntitiy";
    }

    @Override
    public float GetPosX() {
        return xPos;
    }

    @Override
    public float GetPosY() {
        return yPos;
    }

    @Override
    public float GetRadius() {
        return ((bmp[m_SpriteIndex].getWidth() > bmp[m_SpriteIndex].getHeight()) ? bmp[m_SpriteIndex].getWidth() : bmp[m_SpriteIndex].getHeight()) * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other) {
        if (_other instanceof ProjectileEntity) {

            ProjectileEntity projectile = (ProjectileEntity) _other;

            // Player projectile
            if (!projectile.isEnemyProjectile) {
                m_CurrentHealth -= SceneGamePlay.Instance.m_PlayerInfo.GetDamage();
                projectile.SetIsDone(true); // destroy the projectile
            }

        }
    }

    @Override
    public void OnTouch() {
    }

    @Override
    public float GetCurrentHP() {
        return m_CurrentHealth;
    }
}
