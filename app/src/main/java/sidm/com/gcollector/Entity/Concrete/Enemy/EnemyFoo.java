package sidm.com.gcollector.Entity.Concrete.Enemy;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;
import android.widget.ProgressBar;

import sidm.com.gcollector.Entity.Concrete.Projectile.ProjectileEntity;
import sidm.com.gcollector.Constants.LayerConstants;
import sidm.com.gcollector.Entity.EnemyBase;
import sidm.com.gcollector.Entity.EntityBase;
import sidm.com.gcollector.Entity.EntityManager;
import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.Physics.Collidable;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;

// Author: Hui Sheng
// Enemy Berserkie: Throws wooden balls at random direction
public class EnemyFoo implements EntityBase, Collidable, EnemyBase {

    float m_CurrentHealth = 100;

    private Bitmap[] bmp = new Bitmap[4];
    private boolean isDone = false;
    private float xPos, yPos, xDir, yDir, lifeTime;
    private int m_SpriteIndex = 0;
    private float m_SpriteTimer = 0;

    // Helpers
    final float m_ShootInterval = 1.25f;
    float m_NextShootTime;
    int m_AmmoLeft;

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {
        bmp[0] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.enemy1);
        bmp[1] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.enemy12);
        bmp[2] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.enemy13);
        bmp[3] = BitmapFactory.decodeResource(_view.getResources(), R.drawable.enemy14);

        xPos = _view.getWidth() * 0.85f;
        yPos = _view.getHeight() * 0.8f;
    }

    @Override
    public void Update(float _dt) {

        // Enemy's turn time to attack!
        if (!SceneGamePlay.Instance.IsPlayerTurn() && !SceneGamePlay.Instance.m_PlayerInfo.IsWaiting()) {
            m_SpriteTimer += _dt * 8; // Sprite animation speed
            m_SpriteIndex = (int) (m_SpriteTimer % 4); // modulo by max sprite count

            if (SceneGamePlay.Instance.m_ElapsedTime >= m_NextShootTime) {

                // Add cooldown
                m_NextShootTime = SceneGamePlay.Instance.m_ElapsedTime + m_ShootInterval;

                // Play audio
                AudioManager.Instance.PlayAudio(R.raw.missile, 1);

                // Spawn projectile
                EntityManager.Instance.AddEntity(new ProjectileEntity(
                        ProjectileEntity.PROJECTILE_TYPE.ROCKET_SPIKE,
                        SceneGamePlay.Instance.m_PlayerInfo.m_Player.GetPosX(),
                        0,
                        0,
                        0,
                        true
                ));

                m_AmmoLeft--;

                if (m_AmmoLeft <= 0) {
                    SceneGamePlay.Instance.m_PlayerInfo.WaitFor(4);
                    m_SpriteIndex = 0; // reset sprite index when not attacking
                }

            }

        } else {
            m_AmmoLeft = 5; // Reset ammo
        }

    }

    @Override
    public void Render(Canvas _canvas) {

        // Update Progress Bar
        ProgressBar bar = SceneGamePlay.Instance.m_Context.findViewById(R.id.enemyhp);
        bar.setProgress((int) m_CurrentHealth);

        _canvas.drawBitmap(bmp[m_SpriteIndex], xPos - bmp[m_SpriteIndex].getWidth() * 0.5f, yPos - bmp[m_SpriteIndex].getHeight() * 0.5f, null);

    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.LAYER_ENTITY;
    }

    @Override
    public String GetType() {
        return "FooEntity";
    }

    @Override
    public float GetPosX() {
        return xPos;
    }

    @Override
    public float GetPosY() {
        return yPos;
    }

    @Override
    public float GetRadius() {
        return ((bmp[m_SpriteIndex].getWidth() > bmp[m_SpriteIndex].getHeight()) ? bmp[m_SpriteIndex].getWidth() : bmp[m_SpriteIndex].getHeight()) * 0.5f;
    }


    @Override
    public void OnHit(Collidable _other) {
        if (_other instanceof ProjectileEntity) {

            ProjectileEntity projectile = (ProjectileEntity) _other;

            // Player projectile
            if (!projectile.isEnemyProjectile) {
                m_CurrentHealth -= SceneGamePlay.Instance.m_PlayerInfo.GetDamage();
                projectile.SetIsDone(true); // destroy the projectile
                AudioManager.Instance.PlayAudio(R.raw.hit, 1);
            }

        }
    }

    @Override
    public void OnTouch() {
    }

    @Override
    public float GetCurrentHP() {
        return m_CurrentHealth;
    }
}
