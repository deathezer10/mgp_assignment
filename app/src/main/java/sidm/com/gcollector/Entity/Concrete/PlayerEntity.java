package sidm.com.gcollector.Entity.Concrete;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Vibrator;
import android.view.SurfaceView;

import sidm.com.gcollector.Entity.Concrete.Projectile.ProjectileEntity;
import sidm.com.gcollector.Constants.LayerConstants;
import sidm.com.gcollector.Entity.EntityBase;
import sidm.com.gcollector.Entity.EntityManager;
import sidm.com.gcollector.IO.AudioManager;
import sidm.com.gcollector.IO.TouchManager;
import sidm.com.gcollector.Physics.Collidable;
import sidm.com.gcollector.Physics.MyMath;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;


// Author: Hui Sheng
// Player's sprite collision and texture rendering
public class PlayerEntity implements EntityBase, Collidable {


    private Bitmap bmp = null;
    private Bitmap leftarrowbmp = null;
    private Bitmap rightarrowbmp = null;
    private boolean isDone = false;
    private float xPos, yPos, xDir, yDir, lifeTime;
    private SurfaceView m_view;

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {
        m_view = _view;

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.catapult);
        bmp = Bitmap.createScaledBitmap(bmp, _view.getWidth() / 10, _view.getHeight() / 7, false); // Resize to fit screen

        leftarrowbmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.leftarrow);
        rightarrowbmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rightarrow);

        xPos = _view.getWidth() * 0.15f;
        yPos = _view.getHeight() * 0.8f;

    }

    @Override
    public void Update(float _dt) {

    }

    @Override
    public void Render(Canvas _canvas) {

        if (!SceneGamePlay.Instance.IsPlayerTurn()) {
            _canvas.drawBitmap(leftarrowbmp, xPos - leftarrowbmp.getWidth() * 2, yPos - leftarrowbmp.getHeight() * 0.5f, null);
            _canvas.drawBitmap(rightarrowbmp, xPos + rightarrowbmp.getWidth(), yPos - rightarrowbmp.getHeight() * 0.5f, null);
        }

        _canvas.drawBitmap(bmp, xPos - bmp.getWidth() * 0.5f, yPos - bmp.getHeight() * 0.5f, null);
    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.LAYER_ENTITY;
    }

    public static PlayerEntity Create() {
        PlayerEntity result = new PlayerEntity();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public String GetType() {
        return "PlayerEntity";
    }

    @Override
    public float GetPosX() {
        return xPos;
    }

    @Override
    public float GetPosY() {
        return yPos;
    }

    @Override
    public float GetRadius() {
        return bmp.getWidth() * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other) {

        if (_other instanceof ProjectileEntity) {

            ProjectileEntity projectile = (ProjectileEntity) _other;

            // Enemy hit player
            if (projectile.isEnemyProjectile) {
                AudioManager.Instance.PlayAudio(R.raw.hit, 1);
                SceneGamePlay.Instance.m_PlayerInfo.ReduceHealth(40);
                projectile.SetIsDone(true); // destroy the projectile
                // Vibrate phone
                Vibrator vibrator = (Vibrator) SceneGamePlay.Instance.m_Context.getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(100);
            }

        }
    }

    @Override
    public void OnTouch() {
        if (!SceneGamePlay.Instance.IsPlayerTurn()) {
            xPos = TouchManager.Instance.GetPosX();

            xPos = MyMath.Clamp(xPos, m_view.getWidth() * 0.05f, m_view.getWidth() * 0.65f);
        }
    }
}
