package sidm.com.gcollector.Entity;




// Author: Hui Sheng
// Base class for generic Enemies
public interface EnemyBase {

    float GetCurrentHP();

}
