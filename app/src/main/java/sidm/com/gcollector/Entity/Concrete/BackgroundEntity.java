package sidm.com.gcollector.Entity.Concrete;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

import sidm.com.gcollector.Constants.LayerConstants;
import sidm.com.gcollector.Entity.EntityBase;
import sidm.com.gcollector.IO.TouchManager;
import sidm.com.gcollector.R;
import sidm.com.gcollector.Scene.Concrete.SceneGamePlay;

// Author: Hui Sheng
// In game background image
public class BackgroundEntity implements EntityBase {

    private Bitmap bmp = null;
    private boolean isDone = false;

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {
        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.gamescene);
        bmp = Bitmap.createScaledBitmap(bmp, _view.getWidth(), _view.getHeight() , false); // Resize to fit screen
    }

    @Override
    public void Update(float _dt) {

        // Scroll the canvas
        if (false && TouchManager.Instance.HasMoved() && SceneGamePlay.Instance.IsScrollable) { // Uncomment to enable scrolling
            SceneGamePlay.Instance.m_CanvasOffsetX += TouchManager.Instance.GetDeltaPosX();

            // Limit offset to fit the background
            if (SceneGamePlay.Instance.m_CanvasOffsetX > 0)
                SceneGamePlay.Instance.m_CanvasOffsetX = 0;
            else if (SceneGamePlay.Instance.m_CanvasOffsetX < -bmp.getWidth() / 3)
                SceneGamePlay.Instance.m_CanvasOffsetX = -bmp.getWidth() / 3;

            TouchManager.Instance.UpdateXOffset(-SceneGamePlay.Instance.m_CanvasOffsetX); // negative due to offset moving the canvas left instead of right
        }

    }

    @Override
    public void Render(Canvas _canvas) {
        _canvas.drawBitmap(bmp, 0, 0, null);
    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.LAYER_BACKGROUND;
    }
}
