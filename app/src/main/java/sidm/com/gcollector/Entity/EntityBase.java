package sidm.com.gcollector.Entity;


import android.graphics.Canvas;
import android.view.SurfaceView;

// Author: Hui Sheng
// Base class for generic Entities
public interface EntityBase {

    boolean IsDone();
    void SetIsDone(boolean _isDone);

    void Init(SurfaceView _view);
    void Update(float _dt);
    void Render(Canvas _canvas);

    int GetRenderLayer();

}
